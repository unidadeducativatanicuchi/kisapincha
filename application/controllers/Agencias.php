<?php
class Agencias extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Agencia");
        $this->load->library('session');
    }

    public function index()
    {
        $data["listadoAgencias"] = $this->Agencia->consultarTodos();
        $this->load->view("header");
        $this->load->view("agencias/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_agencia)
    {
        $this->Agencia->eliminar($id_agencia);
        $this->session->set_flashdata("confirmacion", "Ciudad eliminada exitosamente");
        redirect("agencias/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("agencias/nuevo");
        $this->load->view("footer");
    }

    public function mapa()
    {
      $data["listadoAgencias"]=$this->Agencia->consultarTodos();//crear array-oposicion
        $this->load->view("header");
        $this->load->view("agencias/mapa",$data);
        $this->load->view("footer");
    }

    public function guardarAgencia()
    {
        $datosNuevaAgencia = array(
            "nombre" => $this->input->post("nombre"),
            "id_ciudad" => $this->input->post("id_ciudad"),
            "direccion" => $this->input->post("direccion"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        $this->Agencia->insertar($datosNuevaAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia guardada exitosamente");
        redirect('agencias/index');
    }

    public function editar($id_agencia)
    {
        $data["agenciaEditar"] = $this->Agencia->obtenerPorId($id_agencia);
        $this->load->view("header");
        $this->load->view("agencias/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarAgencia()
    {
        $id_agencia = $this->input->post("id_agencia");
        $datosAgencia = array(
            "nombre" => $this->input->post("nombre"),
            "id_ciudad" => $this->input->post("id_ciudad"),
            "direccion" => $this->input->post("direccion"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        $this->Agencia->actualizar($id_agencia, $datosAgencia);
        $this->session->set_flashdata("confirmacion", "Agencia actualizada exitosamente");
        redirect('agencias/index');
    }
}
?>

<?php
class Cajeros extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Cajero"); // Se carga el modelo Cajero
        $this->load->library('session');
    }

    public function index()
    {
        $data["listadoCajeros"] = $this->Cajero->consultarTodos();
        $this->load->view("header");
        $this->load->view("cajeros/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_cajero)
    {
        $this->Cajero->eliminar($id_cajero);
        $this->session->set_flashdata("confirmacion", "Cajero eliminado exitosamente"); // Se corrigió el mensaje de confirmación
        redirect("cajeros/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("cajeros/nuevo");
        $this->load->view("footer");
    }

    public function reporte()
    {
      $data["listadoCajeros"]=$this->Cajero->consultarTodos();
        $this->load->view("header");
        $this->load->view("cajeros/reporte",$data);
        $this->load->view("footer");
    }

    public function guardarCajero()
    {
        $datosNuevoCajero = array(
            "ubicacion" => $this->input->post("ubicacion"),
            "id_agencia" => $this->input->post("id_agencia"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        $this->Cajero->insertar($datosNuevoCajero);
        $this->session->set_flashdata("confirmacion", "Cajero guardado exitosamente"); // Se corrigió el mensaje de confirmación
        redirect('cajeros/index');
    }

    public function editar($id_cajero)
    {
        $data["cajeroEditar"] = $this->Cajero->obtenerPorId($id_cajero); // Se carga el cajero desde el modelo Cajero
        $this->load->view("header");
        $this->load->view("cajeros/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCajero()
    {
        $id_cajero = $this->input->post("id_cajero");
        $datosCajero = array(
            "ubicacion" => $this->input->post("ubicacion"),
            "id_agencia" => $this->input->post("id_agencia"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        $this->Cajero->actualizar($id_cajero, $datosCajero);
        $this->session->set_flashdata("confirmacion", "Cajero actualizado exitosamente"); // Se corrigió el mensaje de confirmación
        redirect('cajeros/index');
    }
}
?>

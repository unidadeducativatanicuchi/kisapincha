<?php
class Reportes extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Ciudad");
        $this->load->model("Agencia");
       
    }

    public function index()
    {
        $data["ciudades"] = $this->Ciudad->consultarTodos();
        $data["agencias"] = $this->Agencia->consultarTodos();

        $this->load->view("header");
        $this->load->view("reportes/index", $data);
        $this->load->view("footer");
    }

    
}
?>

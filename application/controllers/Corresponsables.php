<?php

class Corresponsables extends CI_Controller
{

  function __construct()
  {
      parent::__construct();
      $this->load->model("Corresponsable");
      $this->load->model('Ciudad');
      $this->load->database();
      $this->load->library('session'); // Cargar la biblioteca de sesiones
  }
  public function Ciudad() {
      // Obtener datos de las ciudades desde el modelo Ciudad
      $data['listadoCiudades'] = $this->Ciudad->obtener_ciudades(); // Asegúrate de tener un método obtener_ciudades en tu modelo Ciudad_model
      // Cargar la vista con los datos
      $this->load->view('reporteG', $data);
  }
  public function index()
  {
    $data["listadoCorresponsables"] = $this->Corresponsable->consultarTodos(); // Aquí se llama a los métodos del modelo Corresponsable

    $this->load->view("header");
    $this->load->view("corresponsables/index", $data);
    $this->load->view("footer");
  }

  public function reporte() {
      // Aquí obtén los datos necesarios para el reporte, por ejemplo, $listadoCorresponsables
      $data['listadoCorresponsables'] = $this->Corresponsable->consultarTodos(); // Cambiado para obtener los corresponsables del modelo

      // Luego carga la vista pasando los datos
      $this->load->view('header');
      $this->load->view('corresponsables/reporte', $data);
      $this->load->view('footer');
  }
  public function reporteG() {
      // Aquí obtén los datos necesarios para el reporte, por ejemplo, $listadoCorresponsables
      $data['listadoCorresponsables'] = $this->Corresponsable->consultarTodos(); // Cambiado para obtener los corresponsables del modelo

      // Luego carga la vista pasando los datos
      $this->load->view('header');
      $this->load->view('corresponsables/reporteG', $data);
      $this->load->view('footer');

  }

  public function borrar($id_corresponsable)
  {
    $this->Corresponsable->eliminar($id_corresponsable);
    $this->session->set_flashdata("confirmacion", "Corresponsal eliminado exitosamente");

    redirect("corresponsables/index");
  }

  public function editar($id)
  {
    $data["corresponsalEditar"] = $this->Corresponsable->obtenerPorId($id);
    $this->load->view("header");
    $this->load->view("corresponsables/editar", $data);
    $this->load->view("footer");
  }

  public function nuevo()
  {
    $this->load->view("header");
    $this->load->view("corresponsables/nuevo");
    $this->load->view("footer");
  }

  public function insertarCorresponsal()
  {
    $datosNuevoCorresponsal = array(
      "nombre" => $this->input->post("nombre"),
      "telefono" => $this->input->post("telefono"),
      "latitud" => $this->input->post("latitud"),
      "longitud" => $this->input->post("longitud"),
    );
    $this->Corresponsable->insertar($datosNuevoCorresponsal);
    $this->session->set_flashdata("confirmacion", "Corresponsal guardado exitosamente");

    redirect('corresponsables/index');
  }

  public function guardarCorresponsable()
  {
    $datosNuevoCorresponsal = array(
        "nombre" => $this->input->post("nombre"),
        "telefono" => $this->input->post("telefono"),
        "latitud" => $this->input->post("latitud"),
        "longitud" => $this->input->post("longitud"),
    );
    $this->Corresponsable->insertar($datosNuevoCorresponsal);
    $this->session->set_flashdata("confirmacion", "Corresponsal guardado exitosamente");

    redirect('corresponsables/index');
  }

  public function actualizarCorresponsal()
  {
    $id_corresponsal = $this->input->post("id_corresponsable"); // Corregido el nombre del campo
    $datosCorresponsal = array(
        "nombre" => $this->input->post("nombre"),
        "telefono" => $this->input->post("telefono"),
        "latitud" => $this->input->post("latitud"),
        "longitud" => $this->input->post("longitud")
    );
    $this->Corresponsable->actualizar($id_corresponsal, $datosCorresponsal);
    $this->session->set_flashdata("confirmacion", "Corresponsal actualizado exitosamente");
    redirect('corresponsables/index');
  }
}
?>

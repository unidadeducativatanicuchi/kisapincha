<?php
class Ciudades extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Ciudad");
        $this->load->library('session');
    }

    public function index()
    {
        $data["listadoCiudades"] = $this->Ciudad->consultarTodos();
        $this->load->view("header");
        $this->load->view("ciudades/index", $data);
        $this->load->view("footer");
    }

    public function borrar($id_ciudad)
    {
        $this->Ciudad->eliminar($id_ciudad);
        $this->session->set_flashdata("confirmacion", "Ciudad eliminada exitosamente");
        redirect("ciudades/index");
    }

    public function nuevo()
    {
        $this->load->view("header");
        $this->load->view("ciudades/nuevo");
        $this->load->view("footer");
    }


    public function mapa()
    {
      $data["listadoCiudades"]=$this->Ciudad->consultarTodos();//crear array-oposicion
        $this->load->view("header");
        $this->load->view("ciudades/mapa",$data);
        $this->load->view("footer");
    }


    public function guardarCiudad()
    {
        $datosNuevaCiudad = array(
            "nombre" => $this->input->post("nombre"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        $this->Ciudad->insertar($datosNuevaCiudad);
        $this->session->set_flashdata("confirmacion", "Ciudad guardada exitosamente");
        redirect('ciudades/index');
    }

    public function editar($id_ciudad)
    {
        $data["ciudadEditar"] = $this->Ciudad->obtenerPorId($id_ciudad);
        $this->load->view("header");
        $this->load->view("ciudades/editar", $data);
        $this->load->view("footer");
    }

    public function actualizarCiudad()
    {
        $id_ciudad = $this->input->post("id_ciudad");
        $datosCiudad = array(
            "nombre" => $this->input->post("nombre"),
            "latitud" => $this->input->post("latitud"),
            "longitud" => $this->input->post("longitud")
        );
        $this->Ciudad->actualizar($id_ciudad, $datosCiudad);
        $this->session->set_flashdata("confirmacion", "Ciudad actualizada exitosamente");
        redirect('ciudades/index');
    }
}
?>

<?php
class Agencia extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insertar($datos)
    {
        $agencias = $this->db->insert("Agencias", $datos);
        return $agencias;
    }

    function consultarTodos()
    {
        $agencias= $this->db->get("Agencias");
        if ($agencias->num_rows() > 0) {
            return $agencias->result();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {
        $this->db->where("id_agencia", $id);
        return $this->db->delete("Agencias");
    }

    function obtenerPorId($id)
    {
        $this->db->where("id_agencia", $id);
        $agencia = $this->db->get("Agencias");
        if ($agencia->num_rows() > 0) {
            return $agencia->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where("id_agencia", $id);
        return $this->db->update("Agencias", $datos);
    }
}
?>

<?php
class Corresponsable extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    // insertar nuevos corresponsales
    function insertar($datos)
    {
        $respuesta = $this->db->insert("Corresponsables", $datos);
        return $respuesta;
    }

    // consulta de datos
    function consultarTodos()
    {
        $corresponsables = $this->db->get("Corresponsables");
        if ($corresponsables->num_rows() > 0) {
            return $corresponsables->result();
        } else {
            return false;
        }
    }

    // eliminación de corresponsal por id
    function eliminar($id)
    {
        $this->db->where("id_corresponsable", $id);
        return $this->db->delete("Corresponsables");
    }

    // consulta de un solo corresponsal por id
    function obtenerPorId($id)
    {
        $this->db->where("id_corresponsable", $id);
        $corresponsal = $this->db->get("Corresponsables");
        if ($corresponsal->num_rows() > 0) {
            return $corresponsal->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where("id_corresponsable", $id);
        return $this->db->update("Corresponsables", $datos);
    }
}

// fin de la clase
?>

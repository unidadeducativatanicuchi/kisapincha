<?php
class Cajero extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insertar($datos)
    {
        // Insertar datos en la tabla Cajeros
        $cajeros = $this->db->insert("Cajeros", $datos);
        return $cajeros;
    }

    function consultarTodos()
    {
        // Consultar todos los registros de la tabla Cajeros
        $cajeros = $this->db->get("Cajeros");
        if ($cajeros->num_rows() > 0) {
            return $cajeros->result();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {
        // Eliminar un registro de la tabla Cajeros por su ID
        $this->db->where("id_cajero", $id);
        return $this->db->delete("Cajeros");
    }

    function obtenerPorId($id)
    {
        // Obtener un registro de la tabla Cajeros por su ID
        $this->db->where("id_cajero", $id);
        $cajero = $this->db->get("Cajeros");
        if ($cajero->num_rows() > 0) {
            return $cajero->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        // Actualizar un registro de la tabla Cajeros por su ID
        $this->db->where("id_cajero", $id);
        return $this->db->update("Cajeros", $datos);
    }
}
?>

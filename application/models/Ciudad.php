<?php
class Ciudad extends CI_Model
{
    function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    function insertar($datos)
    {
        $respuesta = $this->db->insert("Ciudades", $datos);
        return $respuesta;
    }

    function consultarTodos()
    {
        $ciudades = $this->db->get("Ciudades");
        if ($ciudades->num_rows() > 0) {
            return $ciudades->result();
        } else {
            return false;
        }
    }

    function eliminar($id)
    {
        $this->db->where("id_ciudad", $id);
        return $this->db->delete("Ciudades");
    }

    function obtenerPorId($id)
    {
        $this->db->where("id_ciudad", $id);
        $ciudad = $this->db->get("Ciudades");
        if ($ciudad->num_rows() > 0) {
            return $ciudad->row();
        } else {
            return false;
        }
    }

    function actualizar($id, $datos)
    {
        $this->db->where("id_ciudad", $id);
        return $this->db->update("Ciudades", $datos);
    }
}
?>

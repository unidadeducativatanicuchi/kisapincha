<h1>EDITAR CAJEROS</h1>
<form class="" method="post" action="<?php echo site_url('cajeros/actualizarCajero'); ?>">
	<input type="hidden" name="id_cajero" id="id_cajero"
	value="<?php echo $cajeroEditar->id_cajero; ?>">


<div class="row">
	<div class="col-md-1">
	</div>
	<div class="col-md-4">
	</label>
		<b>Direccion:</b>
		</label>
		<input type="text" name="ubicacion" id="ubicacion"
		value="<?php echo $agenciaEditar->nombre; ?>"
		placeholder="Ingrese la direccion." class="form-control" oninput="this.value = this.value.replace(/[^a-zA-Z]/g, '')" required>
	</div>
	<div class="col-md-1">
	</div>
	<div class="col-md-4">
		<label>
			<b>ID_AGENCIA:</b>
			</label>
			<input type="text" name="id_agencia" id="id_agencia"
			value="<?php echo $cajeroEditar->id_agencia; ?>"
			placeholder="Ingrese la agencia" class="form-control" required>

	</div>
</div>



<div class="row">
	<div class="col-md-1">
	</div>
</div>
<div class="row">
	<div class="col-md-4">
		<label for="">
		<b>Latitud:</b>
	</label>
	<input type="number" name="latitud" id="latitud"
	value="<?php echo $cajeroEditar->latitud; ?>"
	placeholder="Ingrese el latitud.." class="form-control" readonly>

	</div>
	<div class="col-md-1">
	</div>
	<div class="col-md-4">
		<label for="">
		<b>Longitud:</b>
	</label>
	<input type="number" name="longitud" id="longitud"
	value="<?php echo $cajeroEditar->longitud; ?>"
	placeholder="Ingrese el longitud..." class="form-control" readonly>

	</div>

</div>

<br>

    <div class="row">
      <div class="col-md-12">
        <div id="mapa1" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-
spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $cajeroEditar->latitud; ?>, <?php echo $cajeroEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa1'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>

<!DOCTYPE html>
<html>
<head>
  <title>Mapa</title>
  <script src="https://maps.googleapis.com/maps/api/js?key=TU_CLAVE_DE_API&callback=initMap" async defer></script>
  <style>
    #mapa {
      height: 300px;
      width: 100%;
      border: 1px solid black;
    }
  </style>
</head>
<body>

<h1>
  <b>
    <i class="fa fa-plus-home"></i>
    NUEVO CAJERO
  </b>
</h1>

<form class="" action="<?php echo site_url('cajeros/guardarCajero'); ?>" method="post" enctype="multipart/form-data">
  <div class="row">
    <div class="col-md-4">
      <label for=""><b>Dirección:</b></label>
      <input type="text" name="ubicacion" id="ubicacion" class="form-control" placeholder="Ingrese la ubicación"><br>
    </div>
    <div class="col-md-4">
      <label for=""><b>ID_Agencia:</b></label>
      <!-- Deberías llenar este campo con las opciones disponibles de agencias -->
      <select name="id_agencia" id="id_agencia" class="form-control">
        <option value="">Seleccione una agencia</option>
        <!-- Aquí debes agregar las opciones disponibles de agencias -->
      </select>
      <br>
    </div>
  </div>

  <!-- Campos de entrada para latitud y longitud -->
  <div class="row">
    <div class="col-md-4">
      <label for=""><b>Latitud:</b></label>
      <input type="text" name="latitud" id="latitud" class="form-control" placeholder="Latitud" readonly>
    </div>
    <div class="col-md-4">
      <label for=""><b>Longitud:</b></label>
      <input type="text" name="longitud" id="longitud" class="form-control" placeholder="Longitud" readonly>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <br>
      <div id="mapa"></div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk fa-spin"></i>Guardar</button> &nbsp;
      <a href="<?php echo site_url('cajeros/index'); ?>" class="btn btn-danger"><i class="fa-solid fa-ban fa-spin"></i>Cancelar</a>
    </div>
  </div>
</form>

<script type="text/javascript">
  function initMap() {
    var coordenadaCentral = new google.maps.LatLng(-0.15349217572235827, -78.47793881592635);
    var miMapa = new google.maps.Map(document.getElementById('mapa'), {
      center: coordenadaCentral,
      zoom: 8,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Selecciona la ubicación',
      draggable: true
    });

    google.maps.event.addListener(marcador, 'dragend', function(event) {
      var latitud = this.getPosition().lat();
      var longitud = this.getPosition().lng();
      document.getElementById('latitud').value = latitud;
      document.getElementById('longitud').value = longitud;
    });
  }
</script>

</body>
</html>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Cajero</title>
    <!-- Agrega aquí tus enlaces a CSS y scripts JS -->
    <!-- Enlace a la librería Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
    <!-- Enlace a iziToast CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/css/iziToast.min.css">

    <!-- Script de iziToast -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/izitoast/1.4.0/js/iziToast.min.js"></script>
</head>
<body>
    <h1><i class="fa fa-hospital"></i>Cajeros</h1>
    <div class="row">
        <div class="col-md-12 text-end">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-eye"></i> Ver Mapa
            </button>

            <a href="<?php echo site_url('cajeros/nuevo') ?>" class="btn btn-outline-success">
                <i class="fa fa-plus-circle fa-1x"></i> Agregar Cajero
            </a>
        </div>
    </div>
    <br>
    <?php if ($listadoCajeros): ?>
        <table class="table table-bordered" style="border-radius: 10px; margin: 0 20px; width: 80%;">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>UBICACION</th>
                    <th>ID AGENCIA</th>
                    <th>LATITUD</th>
                    <th>LONGITUD</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($listadoCajeros as $cajero): ?>
                    <tr>
                        <td><?php echo $cajero->id_cajero; ?></td>
                        <td><?php echo $cajero->ubicacion; ?></td>
                        <td><?php echo $cajero->id_agencia; ?></td>
                        <td><?php echo $cajero->latitud; ?></td>
                        <td><?php echo $cajero->longitud; ?></td>
                        <td>
                            <a href="<?php echo site_url('cajeros/editar/').$cajero->id_cajero; ?>"
                                 class="btn btn-warning"
                                 title="Editar">
                                <i class="fa fa-pen"></i>
                            </a>
                            <a href="<?php echo site_url('cajeros/borrar/') . $cajero->id_cajero; ?>"
                               class="btn btn-danger delete-confirm" data-id="<?php echo $cajero->id_cajero; ?>">
                                <i class="fa fa-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    <?php else: ?>
        <div class="alert alert-danger">
            No se encontraron cajero
        </div>
    <?php endif; ?>

    <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">
                        <i class="fa fa-eye"></i> Mapa de Cajero
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-bs-dismiss="modal">
                        <i class="fa fa-times"></i>Cerrar
                    </button>
                </div>
            </div>
        </div>
    </div>

    <!-- Marcadores dentro del modal -->
    <script type="text/javascript">
        function initMap() {
            var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
            var miMapa = new google.maps.Map(document.getElementById('mapa1'), {
                center: coordenadaCentral,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            <?php foreach ($listadoCajeros as $cajero): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $cajero->latitud; ?>,
                    <?php echo $cajero->longitud; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: '<?php echo $cajero->ubicacion; ?>',
                });
            <?php endforeach; ?>
        }
    </script>
    <!-- Script para manejar la confirmación de borrado y mostrar mensajes de confirmación -->
<script>
    document.addEventListener('DOMContentLoaded', function () {
        var deleteButtons = document.querySelectorAll('.delete-confirm');
        deleteButtons.forEach(function (button) {
            button.addEventListener('click', function (event) {
                event.preventDefault();
                var id = this.getAttribute('data-id');
                // Mostrar un mensaje de confirmación antes de eliminar
                iziToast.question({
                    timeout: false,
                    close: false,
                    overlay: true,
                    displayMode: 'once',
                    id: 'question',
                    zindex: 999,
                    title: 'Confirmación',
                    message: '¿Está seguro de eliminar este cajero?',
                    position: 'center',
                    buttons: [
                        ['<button><b>Sí</b></button>', function (instance, toast) {
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                            // Aquí deberías hacer la llamada a tu función para eliminar el corresponsal
                            window.location.href = '<?php echo site_url('cajeros/borrar/') ?>' + id;
                        }, true],
                        ['<button>No</button>', function (instance, toast) {
                            instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                        }],
                    ],
                });
            });
        });

        // Mostrar mensaje de confirmación al guardar
        var confirmacionGuardado = '<?php echo $this->session->flashdata("confirmacion"); ?>';
        if (confirmacionGuardado) {
            iziToast.success({
                title: 'Éxito',
                message: confirmacionGuardado,
                position: 'topRight',
            });
        }
    });
</script>

</body>
</html>

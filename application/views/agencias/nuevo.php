<h1>
  <b>
    <i class="fa fa-plus-home"></i>
    NUEVA AGENCIA
  </b>
</h1>
<br>
<form class="" action="<?php echo site_url('agencias/guardarAgencia'); ?>" method="post"
  enctype="multipart/form-data">


<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-4">
    <label for=""><b>Nombre:</b></label>
    <input type="text" name="nombre"  id="nombre" class="form-control" bbb placeholder="Ingrese el nombre" oninput="this.value = this.value.replace(/[^a-zA-Z]/g, '')"><br>
  </div>
  <div class="col-md-1">
  </div>
  <div class="col-md-4">
    <label for="id_ciudad"><b>ID_Ciudad:</b></label>
    <input type="text" name="id_ciudad"  id="id_ciudad" class="form-control" bbb placeholder="Ingrese la ciudad" ><br>

  </div>
</div>


<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-9">
    <center>
    <label for=""><b>Direccion:</b></label>
  </center>
    <input type="text" name="direccion"  id="direccion" class="form-control" bbb placeholder="Ingrese la direccion" oninput="this.value = this.value.replace(/[^a-zA-Z]/g, '')"><br>
  </div>
  <div class="col-md-1">
  </div>
</div>


<div class="row">
  <div class="col-md-1">
  </div>
  <div class="col-md-4">
    <label for=""><b>Latitud:</b></label>
    <input type="number" name="latitud"  id="latitud" class="form-control" bbb placeholder="Ingrese la latitud" readonly>
  </div>
  <div class="col-md-1">
  </div>
  <div class="col-md-4">
    <label for=""><b>Longitud:</b></label>
    <input type="text" name="longitud"  id="longitud" class="form-control" bbb placeholder="Ingrese la longitud" readonly>
  </div>
</div>


<!--inicio de mapa-->
  <br>
    <div class="row">
      <div class="col-md-12">
        <br>
        <div id="mapa" style="height:300px; width:100%; border:1px solid black;"></div>
      </div>
    </div>
  <div class="row">
    <div class="col-md-12 text-center">
      <button type="submit" name="button" class="btn btn-primary"><i class="fa-regular fa-floppy-disk fa-spin"></i>Guardar</button> &nbsp;
      <a href="<?php echo site_url('agencias/index'); ?>"class="btn btn-danger"><i class="fa-solid fa-ban fa-spin"></i>Cancelar</a>

    </div>

  </div>
</form>
<br><br>

<script type="text/javascript">
  function initMap(){ var coordenadaCentral = new google.maps.LatLng(-0.15349217572235827, -78.47793881592635);
  var miMapa=new google.maps.Map(document.getElementById('mapa'),
  {
    center:coordenadaCentral,
    zoom:8,
    mapTypeId:google.maps.MapTypeId.ROADMAP
  });

  var marcador=new google.maps.Marker({
    position:coordenadaCentral,
    map:miMapa,
    title:'Selecciona la ubicacion',
    draggable:true
  });

  google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      //alert(latitud+" -- "+longitud);
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
  );

  }
</script>

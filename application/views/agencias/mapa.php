<h1><i class="fa "></i>Mapa Agencias</h1>
<div id="reporteMapa" style="height:600px; width:100%; border:2px solid black;">

  <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=
            new google.maps.LatLng(-0.152948869329262,
              -78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoAgencias as $agencia): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $agencia->latitud; ?>,
              <?php echo $agencia->longitud; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $agencia->nombre; ?>',
            icon: {
              url: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png' // URL de la imagen de marcador amarillo
            }
          });
        <?php endforeach; ?>

        var infowindow = new google.maps.InfoWindow({
            content: '<img src="https://www.coopdaquilema.com/wp-content/uploads/2018/06/AGENCIA-LATACUNGA.jpg"  width="60px" height="60px"  alt="Descripción de la imagen">'
        });

        // Agregar evento al marcador para mostrar el infowindow cuando se pasa el ratón
        google.maps.event.addListener(marcador, 'mouseover', function() {
            infowindow.open(miMapa, marcador);
        });

        // Cerrar el infowindow cuando el ratón deja el marcador
        google.maps.event.addListener(marcador, 'mouseout', function() {
            infowindow.close();
        });


      }
    </script>

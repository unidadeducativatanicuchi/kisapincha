<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Editable Footer</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css">
</head>
<body>

<!-- Footer Start -->
<div class="container-fluid bg-secondary text-white mt-5 py-5 px-sm-3 px-md-5">
  <div class="row pt-6">
    <div class="col-md-6 text-center">
      <a href="#" class="navbar-brand font-weight-bold text-primary m-0 mb-4 p-0" style="font-size: 40px; line-height: 40px">
        <span class="text-white">Cooperativa de Ahorro <br>
          y  Crédito Kisapincha Ltda.</span>
      </a>
      <br>
      <p id="content">
        Tu Cooperativa te da la oportunidad de crecer, invertir y 
        desarrollar tu negocio,
         a través de un financiamiento, con una baja tasa de interés. 
         Sin requisitos complicados.
      </p>
      <br>
      <br>
      <div class="d-flex justify-content-start mt-4">
        <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px" href="#">
          <i class="fab fa-twitter"></i>
        </a>
        <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px" href="#">
          <i class="fab fa-facebook-f"></i>
        </a>
        <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px" href="#">
          <i class="fab fa-linkedin-in"></i>
        </a>
        <a class="btn btn-outline-primary rounded-circle text-center mr-2 px-0" style="width: 38px; height: 38px" href="#">
          <i class="fab fa-instagram"></i>
        </a>
      </div>
    </div>
    <div class="col-lg-4 col-md-4 text-center">
      <h3 class="text-primary mb-4">CONTACTOS:</h3>
      <div class="d-flex">
        <h4 class="fa fa-map-marker-alt text-primary"></h4>
        <div class="pl-6">
          <h5 class="text-white">DIRECCIÓN:</h5>
          <p id="address">AMBATO-KISAPINCHA</p>
        </div>
      </div>
      <div class="d-flex">
        <h4 class="fa fa-envelope text-primary"></h4>
        <div class="pl-6">
          <h5 class="text-white">CORREO:</h5>
          <p id="email">kisapincha@gmail.com</p>
        </div>
      </div>
      <div class="d-flex">
        <h4 class="fa fa-phone-alt text-primary"></h4>
        <div class="pl-6">
          <h5 class="text-white">TELÉFONO:</h5>
          <p id="phone">098 8269875</p>
        </div>
      </div>
      <button id="editLogoBtn" class="btn btn-primary" data-toggle="modal" data-target="#editModalFooter">
        <i class="fas fa-edit"></i>
      </button>
    </div>
  </div>
</div>
<!-- Footer End -->

<br>
<!-- Edit Modal para el footer -->
<div class="modal fade" id="editModalFooter" tabindex="-1" role="dialog" aria-labelledby="editModalFooterLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="editModalFooterLabel">Editar Footer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editForm">
          <div class="form-group">
            <label for="editAddress">Dirección</label>
            <input type="text" class="form-control" id="editAddress" required>
          </div>
          <div class="form-group">
            <label for="editEmail">Correo Electrónico</label>
            <input type="email" class="form-control" id="editEmail" required>
          </div>
          <div class="form-group">
            <label for="editPhone">Teléfono</label>
            <input type="tel" class="form-control" id="editPhone" pattern="\d{3} \d{7}" required>
            <small>Formato:098 0532485</small>
          </div>
          <div class="form-group">
            <label for="editContent">Contenido</label>
            <textarea class="form-control" id="editContent" rows="3" required></textarea>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <!-- Botón "Guardar" -->
        <button type="button" class="btn btn-primary" onclick="saveChanges()">Guardar Cambios</button>
      </div>
    </div>
  </div>
</div>

<!-- JavaScript Libraries -->
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>


<script>
  // Función para cargar los datos actuales en el modal de edición del footer
  $('#editModalFooter').on('show.bs.modal', function (event) {
    var address = $('#address').text().trim();
    var email = $('#email').text().trim();
    var phone = $('#phone').text().trim();
    var content = $('#content').text().trim();

    $('#editAddress').val(address);
    $('#editEmail').val(email);
    $('#editPhone').val(phone);
    $('#editContent').val(content);
  });

  // Función para guardar los cambios editados
  function saveChanges() {
    var address = $('#editAddress').val();
    var email = $('#editEmail').val();
    var phone = $('#editPhone').val();
    var content = $('#editContent').val();

    if (address === "") {
      alert("Por favor ingrese una dirección.");
      return;
    }

    if (email === "") {
      alert("Por favor ingrese un correo electrónico.");
      return;
    } else if (!isValidEmail(email)) {
      alert("Por favor ingrese un correo electrónico válido.");
      return;
    }

    if (phone === "") {
      alert("Por favor ingrese un número de teléfono.");
      return;
    } else if (!isValidPhone(phone)) {
      alert("Por favor ingrese un número de teléfono válido.");
      return;
    }

    $('#address').text(address);
    $('#email').text(email);
    $('#phone').text(phone);
    $('#content').text(content);

    $('#editModalFooter').modal('hide');
  }

  // Función para validar el formato del correo electrónico
  function isValidEmail(email) {
    var emailRegex = /^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/;
    return emailRegex.test(email);
  }

  // Función para validar el formato del número de teléfono
  function isValidPhone(phone) {
    var phoneRegex = /^\d{3} \d{7}$/;
    return phoneRegex.test(phone);
  }
</script>

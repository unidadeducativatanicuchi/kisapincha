<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Corresponsables</title>
    <!-- Agrega aquí tus enlaces a CSS y scripts JS -->
    <!-- Enlace a la librería Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
</head>
<body>
    <h1><i class="fa fa-hospital"></i>Corresponsables</h1>
    <div class="row">
        <div class="col-md-12 text-end">
            <!-- Botón para agregar un nuevo corresponsal -->
            <a href="<?php echo site_url('corresponsables/nuevo') ?>" class="btn btn-outline-success">
                <i class="fa fa-plus-circle fa-1x"></i> Agregar Corresponsal
            </a>
        </div>
    </div>
    <br>
    <?php if ($listadoCorresponsables): ?>
        <div id="reporteMapa" style="height:400px; width:80%; margin: 0 auto;"></div>
    <?php else: ?>
        <div class="alert alert-danger">
            No se encontraron corresponsables
        </div>
    <?php endif; ?>

    <!-- Marcadores -->
    <script type="text/javascript">
        function initMap() {
            var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
            var miMapa = new google.maps.Map(document.getElementById('reporteMapa'), {
                center: coordenadaCentral,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            <?php foreach ($listadoCorresponsables as $corresponsal): ?>
                var coordenadaTemporal = new google.maps.LatLng(
                    <?php echo $corresponsal->latitud; ?>,
                    <?php echo $corresponsal->longitud; ?>);

                // Crear un marcador personalizado
                var iconoMarcador = {
                    url: '<?php echo base_url("img/lo.png"); ?>', // Ruta a tu imagen de marcador personalizada
                    scaledSize: new google.maps.Size(40, 40), // Tamaño del icono
                    origin: new google.maps.Point(0, 0), // Punto de origen del icono
                    anchor: new google.maps.Point(20, 40) // Punto de anclaje del icono
                };

                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: '<?php echo $corresponsal->nombre; ?>',
                    icon: iconoMarcador // Usar el icono personalizado
                });
            <?php endforeach; ?>
        }
    </script>

    <!-- Script para cargar la API de Google Maps -->
    <script src="https://maps.googleapis.com/maps/api/js?key=TU_API_KEY&callback=initMap" async defer></script>
</body>
</html>

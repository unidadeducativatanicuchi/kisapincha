<div style="border: 2px solid black; padding: 10px; font-size: 14px;">
  <h1>Nuevo Corresponsal</h1>
  <hr>
  <br>
  <form id="formulario-corresponsables" class="" action="<?php echo site_url('corresponsables/guardarCorresponsable'); ?>" method="post" enctype="multipart/form-data">

    <label for=""><b>Nombre:</b></label>
    <input type="text" name="nombre" id="nombre" class="form-control" value="" placeholder="Ingrese el nombre: " required>
    <br>
    <label for=""><b>Teléfono:</b></label>
    <input type="number" name="telefono" id="telefono" class="form-control" value="" placeholder="Ingrese el teléfono: "required>
    <br>
    <br>
    <div class="row">
      <div class="col-md-6">
        <br>
        <label for=""><b>Latitud:</b></label>
        <input type="number" name="latitud" id="latitud" class="form-control" value="" placeholder="Ingrese la latitud" readonly required>

        <label for=""><b>Longitud:</b></label>
        <input type="number" name="longitud" id="longitud" class="form-control" value="" placeholder="Ingrese la longitud" readonly required>
        <br>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height:150px; width:100%; border:1px solid black;"></div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-primary"><i class="fa-solid fa-check fa-bounce"></i>Guardar</button>
        <a href="<?php echo site_url('corresponsables/index'); ?>" class="btn btn-danger"><i class="fa-solid fa-xmark fa-bounce"></i>Cancelar</a>
      </div>
    </div>
  </form>
</div>

<script type="text/javascript">
  function initMap(){
    var coordenadaCentral = new google.maps.LatLng(-0.20513285324039474, -78.4868648138535);
    var miMapa = new google.maps.Map(
      document.getElementById('mapa'),
      {
        center: coordenadaCentral,
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }
    );

    var marcador = new google.maps.Marker({
      position: coordenadaCentral,
      map: miMapa,
      title: 'Selecciona la ubicación',
      draggable: true
    });

    google.maps.event.addListener(
      marcador,
      'dragend',
      function(event){
        var latitud = this.getPosition().lat();
        var longitud = this.getPosition().lng();
        document.getElementById('latitud').value = latitud;
        document.getElementById('longitud').value = longitud;
      }
    );
  }
</script>
<script>
$(document).ready(function(){
    // Configuración de la validación del formulario
    $("#formulario-corresponsal").validate({
        rules: {
            nombre: {
                required: true,
                minlength: 2, // Cambiado a 2 caracteres como mínimo
                lettersonly: true // Asegura que solo letras sean aceptadas
            },
            telefono: {
                required: true,
                digits: true,
                minlength: 10,
                maxlength: 10
            }
        },
        messages: {
            nombre: {
                required: "Por favor, ingrese el nombre",
                minlength: "El nombre debe tener al menos 2 caracteres",
                lettersonly: "Por favor, ingrese solo letras"
            },
            telefono: {
                required: "Por favor, ingrese el teléfono",
                digits: "Por favor, ingrese solo dígitos",
                minlength: "El teléfono debe tener 10 dígitos",
                maxlength: "El teléfono debe tener 10 dígitos"
            }
        }
    });
});
</script>


<script src="https://maps.googleapis.com/maps/api/js?key=TU_API_KEY&callback=initMap" async defer></script>

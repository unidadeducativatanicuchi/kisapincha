<div style="margin: 20px;"> <!-- Agregar un margen de 20px alrededor del formulario -->
    <h1>EDITAR Corresponsable</h1>
    <form class="" method="post" action="<?php echo site_url('corresponsables/actualizarCorresponsal'); ?>">
        <input type="hidden" name="id_corresponsable" id="id_corresponsable" value="<?php echo $corresponsalEditar->id_corresponsable; ?>">
        <br>
        <label for="">
            <b>Nombre:</b>
        </label>
        <input type="text" name="nombre" id="nombre"
            value="<?php echo $corresponsalEditar->nombre; ?>"
            placeholder="Ingrese el nombre..." class="form-control" required>
        <br>
        <br>
        <label for="">
            <b>Telefono:</b>
        </label>
        <input type="text" name="telefono" id="telefono"
            value="<?php echo $corresponsalEditar->telefono; ?>"
            placeholder="Ingrese el telefono..." class="form-control" required>

        <div class="row">
            <div class="col-md-6">
                <br>
                <label for="">
                    <b>Latitud:</b>
                </label>
                <input type="number" name="latitud" id="latitud"
                    value="<?php echo $corresponsalEditar->latitud; ?>"
                    placeholder="Ingrese el latitud..." class="form-control" readonly>

            </div>
            <div class="col-md-6">
                <br>
                <label for="">
                    <b>Longitud:</b>
                </label>
                <input type="number" name="longitud" id="longitud"
                    value="<?php echo $corresponsalEditar->longitud; ?>"
                    placeholder="Ingrese el longitud..." class="form-control" readonly>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div id="mapa" style="height: 250px; width:100%; border:1px solid black;">

                </div>
            </div>
        </div>
        <br>
        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary"><i class="fa fa-floppy-disk fa-bounce"></i> &nbsp Guardar</button> &nbsp &nbsp
                <a href="<?php echo site_url('corresponsables/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-spin"></i> &nbsp Cancelar</a>
            </div>
        </div>
    </form>
</div>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $corresponsalEditar->latitud; ?>, <?php echo $corresponsalEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>

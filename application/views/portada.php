<style>
.text-blue {
color: blue;
}
</style>
<!-- Header Start -->
<div class="container-fluid" style="background-color: #f0f6fd;; padding: 20px 0;">
  <div class="row align-items-center px-3">
    <div class="col-lg-6 text-center text-lg-left">
  <h4 class="text-lead mb-4 mt-5 mt-lg-0"></h4>
  <h1 class="display-3 font-weight-bold text-blue">
    Créditos para toda necesidad
  </h1>
  <p class="text-lead mb-4">
    Tu Cooperativa te da la oportunidad de crecer, invertir y desarrollar
    tu negocio, a través de un financiamiento, con una baja tasa de interés.
    Sin requisitos complicados.
  </p>
</div>
<div class="col-lg-6 text-center text-lg-right" style="position: relative;">
    <img class="assets/img-fluid mt-5" src="https://kisapincha.com/theme-front/assets/img/hero/hero-shape-6.1.png" alt="" style="width: 600px; height: auto;" />
    <img class="assets/img-fluid mt-5" src="https://kisapincha.com/theme-front/assets/img/hero/hero-6.1.png" alt="" style="width: 500px; height: auto; position: absolute; top: 0; left: 0;" />
</div>
<!-- Header End -->
<div class="col-lg-12 text-center">
    <img class="assets/img-fluid mt-5" src="https://kisapincha.com/theme-front/assets/img/breadcrum/team_body.jpg" alt="" style="width: 1000px; height: 400px;" />
</div>
<!-- Header  -->
<div class="col-lg-6 text-center text-lg-left">
<h3 class="display-3 font-weight-bold text-blue">
Microcredito para todas las necesidades.
</h3>
</div>
<div class="col-lg-6 text-center text-lg-right" style="position: relative;">
<img class="assets/img-fluid mt-5" src="https://kisapincha.com/theme-front/assets/img/breadcrum/microcredito2.jpg" alt="" style="width: 500px; height: 300px" />

</div>
<!-- Facilities Start -->
<div class="container-fluid pt-5">
  <div class="container pb-3">
    <div class="row">
      <div class="col-lg-4 col-md-6 pb-1">
        <div
          class="d-flex bg-light shadow-sm border-top rounded mb-4"
          style="padding: 30px"
        >
          <i
            class="flaticon-050-fence h1 font-weight-normal text-primary mb-3"
          ></i>
          <div class="pl-4">
            <h4>Inversiones</h4>
            <p class="m-0">
            </p>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 pb-1">
        <div
          class="d-flex bg-light shadow-sm border-top rounded mb-4"
          style="padding: 30px"
        >
          <i
            class="assets/flaticon-022-drum h1 font-weight-normal text-primary mb-3"
          ></i>
          <div class="pl-4">
            <h4>Ahorro a la vista</h4>
          </div>
        </div>
      </div>
      <div class="col-lg-4 col-md-6 pb-1">
        <div
          class="d-flex bg-light shadow-sm border-top rounded mb-4"
          style="padding: 30px"
        >
          <i
            class="assets/flaticon-030-crayons h1 font-weight-normal text-primary mb-3"
          ></i>
          <div class="pl-4">
            <h4>Ahorro infantil</h4>
          </div>
        </div>
      </div>
      <div class="d-flex flex-wrap justify-content-around">
  <div class="col-lg-4 col-md-6 pb-1">
    <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 30px;">
      <i class="assets/flaticon-030-crayons h1 font-weight-normal text-primary mb-3"></i>
      <div class="pl-4">
        <h4>Ahorro programado</h4>
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-md-6 pb-1">
    <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 30px;">
      <i class="assets/flaticon-030-crayons h1 font-weight-normal text-primary mb-3"></i>
      <div class="pl-4">
        <h4>Crédito de consumo</h4>
      </div>
    </div>
  </div>

  <div class="col-lg-4 col-md-6 pb-1">
    <div class="d-flex bg-light shadow-sm border-top rounded mb-4" style="padding: 30px;">
      <i class="assets/flaticon-030-crayons h1 font-weight-normal text-primary mb-3"></i>
      <div class="pl-4">
        <h4>INVERSIONES</h4>
      </div>
    </div>
  </div>
</div>

    </div>
  </div>
</div>
<!-- Facilities Start -->

$address = $_POST['address'];
<?php
// Conexión a la base de datos
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "kisapincha";

$conn = new mysqli($servername, $username, $password, $dbname);

// Verificar la conexión
if ($conn->connect_error) {
  die("La conexión falló: " . $conn->connect_error);
}

// Obtener los datos del formulario
$address = $_POST['address'];
$email = $_POST['email'];
$phone = $_POST['phone'];
$content = $_POST['content'];

// Preparar la sentencia SQL
$sql = "INSERT INTO footer_data (address, email, phone, content) VALUES (?, ?, ?, ?)";
$stmt = $conn->prepare($sql);
$stmt->bind_param("ssss", $address, $email, $phone, $content);

// Ejecutar la sentencia SQL
if ($stmt->execute() === TRUE) {
  echo "Datos guardados correctamente.";
} else {
  echo "Error al guardar los datos: " . $conn->error;
}

// Cerrar la conexión
$stmt->close();
$conn->close();
?>

<h1><i class="fa "></i>Reporte General</h1>
<div id="reporteMapa" style="height:600px; width:100%; border:2px solid black;">

  <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=
            new google.maps.LatLng(-0.152948869329262,
              -78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($ciudades as $ciudad): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $ciudad->latitud; ?>,
              <?php echo $ciudad->longitud; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $ciudad->nombre; ?>',
            icon:"<?php echo base_url('assets/img/ciudad.svg'); ?>" 

          });
        <?php endforeach; ?>

        <?php foreach ($agencias as $agencia): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $agencia->latitud; ?>,
              <?php echo $agencia->longitud; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $agencia->nombre; ?>',
            icon:"<?php echo base_url('assets/img/agencia.svg'); ?>"


          });
        <?php endforeach; ?>

        // Crear infowindow con contenido personalizado (imagen)

        var infowindow = new google.maps.InfoWindow({
            content: '<img src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/11/f8/65/aa/fachada-del-hotel-cotopaxi.jpg?w=1200&h=-1&s=1"  width="60px" height="60px"  alt="Descripción de la imagen">'
        });

        // Agregar evento al marcador para mostrar el infowindow cuando se pasa el ratón
        google.maps.event.addListener(marcador, 'mouseover', function() {
            infowindow.open(miMapa, marcador);
        });

        // Cerrar el infowindow cuando el ratón deja el marcador
        google.maps.event.addListener(marcador, 'mouseout', function() {
            infowindow.close();
        });
      }
    </script>

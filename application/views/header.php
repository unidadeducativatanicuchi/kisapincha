<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet" />
  <title>KISAPINCHA</title>
  <link rel="icon" href="https://kisapincha.com/theme-front/assets/img/hero/hero-shape-6.1.png" type="image/png">

  <!-- Importacion de Jquery -->
    <script src="https://code.jquery.com/jquery-3.7.1.js" integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4=" crossorigin="anonymous"></script>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
        <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxKneyvn8b9jwvDdDC0EW7IfJMZ5R6At8&libraries=places&callback=initMap">
  	  </script>
      <!--importacion de fontawesome -->
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.1/css/all.min.css" integrity="sha512-DTOQO9RWCH3ppGqcWaEA1BIZOC6xxalwEsw9c2QQeAIftl+Vegovlnee1c9QX4TctnWMn13TZye+giMm8e2LwA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
      <!-- Importacion de sweetalert2 -->
      <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.all.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/sweetalert2@11.10.2/dist/sweetalert2.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>SIG</title>
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
		<!-- Bootstrap JS y dependencias Popper.js y jQuery -->
		<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <style>
    /* Estilos para el modal */
    .modal {
      display: none;
      position: fixed;
      z-index: 1;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      overflow: auto;
      background-color: rgba(0,0,0,0.4);
      padding-top: 60px;
    }

    .modal-content {
      background-color: #fefefe;
      margin: 5% auto;
      padding: 20px;
      border: 1px solid #888;
      width: 50%;
    }

    .close {
      color: #aaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .close:hover,
    .close:focus {
      color: black;
      text-decoration: none;
      cursor: pointer;
    }

    /* Estilo para el input de tipo file */
    #logoFile {
      display: none;
    }

    #customFileUpload {
      display: inline-block;
      padding: 10px 20px;
      background-color: #007bff;
      color: #fff;
      border: none;
      border-radius: 4px;
      cursor: pointer;
    }
  </style>
</head>
<body>
<!-- Navbar Start -->
<div class="container-fluid bg-light position-relative shadow">
  <nav class="navbar navbar-expand-lg bg-light navbar-light py-3 py-lg-0 px-0 px-lg-5">
    <a href="" class="navbar-brand font-weight-bold text-secondary" style="font-size: 50px">
      <img id="logoImg" src="img/favicon.ico" alt="Logo" width="50" height="50">
      <span id="siteName" class="text-primary">Kisapincha</span>
    </a>
    <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
      <div class="navbar-nav font-weight-bold mx-auto py-0">
        <a href="<?php echo site_url(); ?>" class="nav-item nav-link">Inicio</a>

        <li class="nav-item dropdown">
              <a class="nav-link dropdown-toggle" href="<?php echo site_url(); ?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                Ciudad
              </a>
               <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                <li><a class="dropdown-item" href="<?php echo site_url('ciudades/index'); ?>">Lista Ciudades</a></li>
                <li><a class="dropdown-item" href="<?php echo site_url('ciudades/nuevo'); ?>">Agregar</a></li>
                <li><a class="dropdown-item" href="<?php echo site_url('ciudades/mapa'); ?>">Mapa</a></li>
              </ul>
            </li>

            <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="<?php echo site_url(); ?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                    Agencia
                  </a>
                   <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <li><a class="dropdown-item" href="<?php echo site_url('agencias/index'); ?>">Lista Agencias</a></li>
                    <li><a class="dropdown-item" href="<?php echo site_url('agencias/nuevo'); ?>">Agregar</a></li>
                    <li><a class="dropdown-item" href="<?php echo site_url('agencias/mapa'); ?>">Mapa</a></li>
                  </ul>
            </li>

            <li class="nav-item dropdown">
                  <a class="nav-link dropdown-toggle" href="<?php echo site_url(); ?>" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Corresponsales
                  </a>
                  <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <li><a class="dropdown-item" href="<?php echo site_url('corresponsables/nuevo'); ?>">Agregar</a></li>
                  <li><a class="dropdown-item" href="<?php echo site_url('corresponsables/index'); ?>">Listar</a></li>
                  <li><a class="dropdown-item" href="<?php echo site_url('corresponsables/reporte'); ?>">Reporte</a></li>
                      </ul>
            </li>


        <a href="class.html" class="nav-item nav-link">Classes</a>
        <a href="team.html" class="nav-item nav-link">Teachers</a>
        <a href="gallery.html" class="nav-item nav-link">Gallery</a>
        <div class="nav-item dropdown">
          <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Pages</a>
          <div class="dropdown-menu rounded-0 m-0">
            <a href="blog.html" class="dropdown-item">Blog Grid</a>
            <a href="single.html" class="dropdown-item">Blog Detail</a>
          </div>
        </div>
        <a href="<?php echo site_url('reportes/index'); ?>" class="nav-item nav-link">Reportes</a>
      </div>
      <br><br> <br>
      <button id="editLogoBtn" class="btn btn-primary">
        <i class="fas fa-edit"></i>
      </button>
    </div>
  </nav>
</div>
<!-- Navbar End -->

<!-- Modal de edición -->
<div id="editModal" class="modal">
  <div class="modal-content">
    <span id="closeBtn" class="close">&times;</span>
    <h2>Editar Logo y Nombre</h2>
    <!-- Contenedor de la vista previa de la imagen -->
    <div id="imagePreview">
      <img id="previewImg" src="#" alt="Vista previa de la imagen" style="max-width: 100%; max-height: 200px; display: none;">
    </div>
    <label for="logoFile">
      <span id="fileLabel"><i class="fas fa-upload"></i> Subir archivo</span>
    </label>
    <!-- Input type file para cargar el logo -->
    <input type="file" id="logoFile" accept="image/*" onchange="previewImage(event)">
    <label for="siteNameInput">Nombre del Sitio:</label>
    <input type="text" id="siteNameInput">
    <br><br><br>
    <button id="saveChangesBtn" class="btn btn-success">Guardar Cambios</button>
    <span id="updateMessage"></span> <!-- Nuevo elemento para mostrar el mensaje de actualización -->
  </div>
</div>

<script>
function previewImage(event) {
  var reader = new FileReader();
  reader.onload = function(){
    var img = document.getElementById('previewImg');
    img.src = reader.result;
    img.style.display = 'block';
    document.getElementById('fileLabel').textContent = event.target.files[0].name; // Mostrar el nombre del archivo seleccionado
  }
  reader.readAsDataURL(event.target.files[0]);
}
</script>

<script>
  document.addEventListener("DOMContentLoaded", function() {
    // Obtén los elementos del DOM necesarios
    const editModal = document.getElementById('editModal');
    const editBtn = document.getElementById('editLogoBtn');
    const closeBtn = document.getElementById('closeBtn');
    const saveChangesBtn = document.getElementById('saveChangesBtn');
    const logoImg = document.getElementById('logoImg');
    const siteName = document.getElementById('siteName');
    const siteNameInput = document.getElementById('siteNameInput');
    const logoFileInput = document.getElementById('logoFile');
    const updateMessage = document.getElementById('updateMessage');

    // Cuando se haga clic en el botón de editar, muestra el modal
    editBtn.onclick = function() {
      editModal.style.display = 'block';
    }

    // Cuando se haga clic en la "x", cierra el modal
    closeBtn.onclick = function() {
      editModal.style.display = 'none';
    }

    // Cuando se haga clic en guardar cambios, guarda los cambios en localStorage
    saveChangesBtn.onclick = function() {
      const logoFile = logoFileInput.files[0];
      const newName = siteNameInput.value;

      // Verifica si se ha seleccionado un archivo
      if (logoFile) {
        const reader = new FileReader();
        reader.onload = function(e) {
          const logoUrl = e.target.result;
          localStorage.setItem('logoUrl', logoUrl);
          logoImg.src = logoUrl;
        }
        reader.readAsDataURL(logoFile);
      }

      localStorage.setItem('siteName', newName);
      siteName.textContent = newName;

      // Muestra el mensaje de confirmación
      updateMessage.textContent = 'Se actualizó el perfil';

      // Cierra el modal después de 2 segundos y elimina el mensaje de confirmación
      setTimeout(function() {
        editModal.style.display = 'none';
        updateMessage.textContent = '';
      }, 2000);
    }

    // Carga los valores del logo y el nombre del sitio desde localStorage si están disponibles
    const savedLogoUrl = localStorage.getItem('logoUrl');
    const savedSiteName = localStorage.getItem('siteName');

    if (savedLogoUrl && savedSiteName) {
      logoImg.src = savedLogoUrl;
      siteName.textContent = savedSiteName;
      siteNameInput.value = savedSiteName;
    }
  });
</script>

<!DOCTYPE html>
<html>
<head>
    <title>Mapas de Ciudades y Agencias</title>
    <style>
        #reporteMapa {
            height: 600px;
            width: 100%;
            border: 2px solid black;
        }
    </style>
</head>
<body>
    <h1><i class="fa "></i>Mapa Ciudades</h1>
    <div id="mapaCiudades" style="height: 600px; width: 100%; border: 2px solid black;"></div>

    <h1><i class="fa "></i>Mapa Agencias</h1>
    <div id="mapaAgencias" style="height: 600px; width: 100%; border: 2px solid black;"></div>

    <script type="text/javascript">
        function initMapCiudades() {
            var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
            var miMapa = new google.maps.Map(document.getElementById('mapaCiudades'), {
                center: coordenadaCentral,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            <?php foreach ($listadoCiudades as $ciudad): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $ciudad->latitud; ?>, <?php echo $ciudad->longitud; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: '<?php echo $ciudad->nombre; ?>',
                    icon: {
                        url: 'http://maps.google.com/mapfiles/ms/icons/green-dot.png'
                    }
                });

                var infowindow = new google.maps.InfoWindow({
                    content: '<img src="https://dynamic-media-cdn.tripadvisor.com/media/photo-o/11/f8/65/aa/fachada-del-hotel-cotopaxi.jpg?w=1200&h=-1&s=1" width="60px" height="60px" alt="Descripción de la imagen">'
                });

                google.maps.event.addListener(marcador, 'mouseover', function() {
                    infowindow.open(miMapa, marcador);
                });

                google.maps.event.addListener(marcador, 'mouseout', function() {
                    infowindow.close();
                });
            <?php endforeach; ?>
        }

        function initMapAgencias() {
            var coordenadaCentral = new google.maps.LatLng(-0.152948869329262, -78.4868431364856);
            var miMapa = new google.maps.Map(document.getElementById('mapaAgencias'), {
                center: coordenadaCentral,
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            <?php foreach ($listadoAgencias as $agencia): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $agencia->latitud; ?>, <?php echo $agencia->longitud; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    map: miMapa,
                    title: '<?php echo $agencia->nombre; ?>',
                    icon: {
                        url: 'http://maps.google.com/mapfiles/ms/icons/yellow-dot.png'
                    }
                });

                var infowindow = new google.maps.InfoWindow({
                    content: '<img src="https://www.coopdaquilema.com/wp-content/uploads/2018/06/AGENCIA-LATACUNGA.jpg" width="60px" height="60px" alt="Descripción de la imagen">'
                });

                google.maps.event.addListener(marcador, 'mouseover', function() {
                    infowindow.open(miMapa, marcador);
                });

                google.maps.event.addListener(marcador, 'mouseout', function() {
                    infowindow.close();
                });
            <?php endforeach; ?>
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=TU_API_KEY&callback=initMapCiudades"></script>
    <script async defer src="https://maps.googleapis.com/maps/api/js?key=TU_API_KEY&callback=initMapAgencias"></script>
</body>
</html>

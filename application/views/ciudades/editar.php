<h1>EDITAR CIUDAD</h1>
<form class="" method="post" action="<?php echo site_url('ciudades/actualizarCiudad'); ?>">
	<input type="hidden" name="id_ciudad" id="id_ciudad"
	value="<?php echo $ciudadEditar->id_ciudad; ?>">


<div class="row">
	<div class="col-md-1">
	</div>

	<div class="col-md-9">
		<b>Nombre:</b>
		</label>
		<input type="text" name="nombre" id="nombre"
		value="<?php echo $ciudadEditar->nombre; ?>"
		placeholder="Ingrese el nombre..." class="form-control" oninput="this.value = this.value.replace(/[^a-zA-Z]/g, '')" required>
		<br>
	</div>
	<div class="col-md-1">
	</div>
</div>


<div class="row">
	<div class="col-md-1">
	</div>
	<div class="col-md-4">
		<label for="">
		<b>Latitud:</b>
	</label>
	<input type="number" name="latitud" id="latitud"
	value="<?php echo $ciudadEditar->latitud; ?>"
	placeholder="Ingrese el latitud..." class="form-control" readonly>
	</div>
	<div class="col-md-1">
	</div>
	<div class="col-md-4">
		<label for="">
		<b>Longitud:</b>
	</label>
	<input type="number" name="longitud" id="longitud"
	value="<?php echo $ciudadEditar->longitud; ?>"
	placeholder="Ingrese el longitud..." class="form-control" readonly>

	</div>
</div>
<br>
    <div class="row">
      <div class="col-md-12">
        <div id="mapa" style="height: 250px; whidth:100%; border:1px solid black;">

      </div>
      </div>

    </div>
    <br>
    <br>
    <div class="row">
      <div class="col-md-12 text-center">
        <button type="submit" name="button" class="btn btn-warning"><i class="fa fa-pen"></i> &nbsp Actualizar</button> &nbsp &nbsp
        <a href="<?php echo site_url('ciudades/index'); ?>" class="btn btn-danger"> <i class="fa fa-xmark fa-
spin"></i> &nbsp Cancelar</a>

      </div>

    </div>
</form>

<br>
<br>
<script type="text/javaScript">
  function initMap(){
    var coordenadaCentral =
		new google.maps.LatLng(<?php echo $ciudadEditar->latitud; ?>, <?php echo $ciudadEditar->longitud; ?>);
   var miMapa= new google.maps.Map(
     document.getElementById('mapa'),{
       center: coordenadaCentral,
       zoom: 10,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     }
   );
   var marcador= new google.maps.Marker({
     position:coordenadaCentral,
     map: miMapa,
     title: 'Seleccione la ubicacion',
     draggable:true
   });
   google.maps.event.addListener(
    marcador,
    'dragend',
    function(event){
      var latitud=this.getPosition().lat();
      var longitud=this.getPosition().lng();
      document.getElementById('latitud').value=latitud;
      document.getElementById('longitud').value=longitud;
    }
   );
  }

</script>

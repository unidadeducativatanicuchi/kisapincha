<!-- Formulario para agregar una nueva agencia -->
<form action="<?php echo site_url();?>/ciudades/nuevo" method="post">
    <label for="nombre">Nombre:</label><br>
    <input type="text" id="nombre" name="nombre" required><br>

    <label for="direccion">Dirección:</label><br>
    <input type="text" id="direccion" name="direccion" required><br>

    <label for="ciudad">Ciudad:</label><br>
    <select id="ciudad" name="ciudad" required>
        <option value="">Selecciona una ciudad</option>
        <option value="Ambato">Ambato</option>
        <option value="Pillaro">Pillaro</option>
        <option value="Riobamba">Riobamba</option>
        <option value="Latacunga">Latacunga</option>
    </select><br>

    <label for="latitud">Latitud:</label><br>
    <input type="text" id="latitud" name="latitud" required><br>

    <label for="longitud">Longitud:</label><br>
    <input type="text" id="longitud" name="longitud" required><br>

    <button type="submit">Guardar</button>
</form>

<!-- División para el mapa -->
<div class="row">
    <div class="col-md-12">
        <div id="mapaA" style="height:500px; width:100%; border:2px solid black;"></div>
    </div>
</div>
<!-- Fin de la división -->

<!-- Tabla de agencias -->
<h1 class="text-center">Agencia</h1>

<div class="read_bt2 col-md-3">
    <a href="<?php echo site_url();?>/ciudades/nuevo" title="Nueva Agencia">
        <span class="fa fa-user"></span>Nueva Agencia
    </a>
</div>

<br><br>

<?php if ($agencias): ?>
    <table class="table table-bordered table-hover" style="background-color:#FEF5E7 ;">
        <thead>
            <tr>
                <th>ID</th>
                <th>NOMBRE</th>
                <th>DIRECCION</th>
                <th>NOMBRE CIUDAD</th>
                <th>LATITUD</th>
                <th>LONGITUD</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($agencias as $filaTemporal): ?>
                <tr>
                    <td><?php echo $filaTemporal->id_agencia; ?></td>
                    <td><?php echo $filaTemporal->nombre; ?></td>
                    <td><?php echo $filaTemporal->direccion; ?></td>
                    <td><?php echo $filaTemporal->nombre_ci; ?></td>
                    <td><?php echo $filaTemporal->latitud; ?></td>
                    <td><?php echo $filaTemporal->longitud; ?></td>
                    <td class="text-center">
                        <a href="<?php echo site_url();?>/ciudades/editar/<?php echo $filaTemporal->id_agencia;?>" title="Editar Agencia" style="color:blue">Editar</a>
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        <a href="<?php echo site_url();?>/ciudades/eliminar/<?php echo $filaTemporal->id_agencia;?>" title="Eliminar Agencia" style="color:red">Eliminar</a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
<?php else: ?>
    <h1>No hay datos</h1>
<?php endif; ?>

<!-- Función para llamar datos tipo coordenada -->
<script type="text/javascript">
    function initMap(){
        var centro = new google.maps.LatLng(-1.509536, -79.398414);
        var mapaTodosAgencias = new google.maps.Map(
            document.getElementById('mapaA'),
            {
                center: centro,
                zoom: 3,
                mapTypeId: google.maps.MapTypeId.HYBRID
            }
        );
        <?php if ($agencias): ?>
    <!-- Función para llamar datos tipo coordenada -->
    <script type="text/javascript">
        function initMap(){
            var centro = new google.maps.LatLng(-1.509536, -79.398414);
            var mapaTodosAgencias = new google.maps.Map(
                document.getElementById('mapaA'),
                {
                    center: centro,
                    zoom: 3,
                    mapTypeId: google.maps.MapTypeId.HYBRID
                }
            );
            <?php foreach($agencias as $lugarTemporal): ?>
                var coordenadaTemporal = new google.maps.LatLng(<?php echo $lugarTemporal->latitud;?>, <?php echo $lugarTemporal->longitud; ?>);
                var marcador = new google.maps.Marker({
                    position: coordenadaTemporal,
                    title: "<?php echo $lugarTemporal->nombre; ?>",
                    map: mapaTodosAgencias,
                    icon: "<?php echo base_url();?>/img/<?php echo $lugarTemporal->nombre_ci;?>.png"
                });
            <?php endforeach; ?>
        }
    </script>
<?php endif; ?>

    }
</script>

<h1><i class="fa fa-home"></i>CIUDADES</h1>
  <div class="row">
    <div class="col-md-12 text-end">
      <!-- Button trigger modal -->
        <!-- <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
        <i class="fa fa-eye"></i> Ver mapa
      </button>
      <a href="<?php echo site_url('Ciudades/nuevo'); ?>" class="btn btn-outline-success"> <i class="fa fa-plus-circle fa-1x"></i>Agregar Ciudad</a>
      <br><br>
    </div> -->
</div>

<?php if ($listadoCiudades): ?>
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>ID</th>
        <th>NOMBRE</th>
        <th>LATITUD</th>
        <th>LONGITUD</th>
        <th>ACCIONES</th>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($listadoCiudades as $ciudad): ?>
        <tr>
          <td> <?php echo $ciudad->id_ciudad; ?></td>
          <td> <?php echo $ciudad->nombre; ?></td>
          <td> <?php echo $ciudad->latitud; ?></td>
          <td> <?php echo $ciudad->longitud; ?></td>


          <td>
            <a href="<?php echo site_url('ciudades/editar/').$ciudad->id_ciudad; ?>" class="btn btn-warning" title="Editar"> <i class="fa fa-pen"></i> </a>
            <a href="<?php echo site_url('ciudades/borrar/').$ciudad->id_ciudad?>" class="btn btn-danger" title="Eliminar"> <i class="fa-solid fa fa-trash"></i> </a>
          </td>
        </tr>
      <?php endforeach;?>
    </tbody>
  </table>


  <!-- Modal -->
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">
            <i class="fa fa-eye"></i>Mapa de Ciudades
          </h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
          <div id="reporteMapa" style="height:300px; width:100%; border:2px solid black;">

          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" data-bs-dismiss="modal"> <i class="fa fa-times"></i> Cerrar</button>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">
      function initMap(){
        var coordenadaCentral=
            new google.maps.LatLng(-0.152948869329262,
              -78.4868431364856);
        var miMapa=new google.maps.Map(
          document.getElementById('reporteMapa'),
          {
            center:coordenadaCentral,
            zoom:8,
            mapTypeId:google.maps.MapTypeId.ROADMAP
          }
        );
        <?php foreach ($listadoCiudades as $ciudad): ?>
        var coordenadaTemporal=
            new google.maps.LatLng(
              <?php echo $ciudad->latitud; ?>,
              <?php echo $ciudad->longitud; ?>);
          var marcador=new google.maps.Marker({
            position:coordenadaTemporal,
            map:miMapa,
            title:'<?php echo $ciudad->nombre; ?>',
          });
        <?php endforeach; ?>

      }
    </script>
<?php else: ?>
  <div class="alert alert-danger">
    No se encontro ciudades registrados

  </div>

<?php endif; ?>
